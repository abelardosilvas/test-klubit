import {Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

export interface FileData {
  name: string;
  size: string;
}

@Component({
  selector: 'app-image-loader',
  templateUrl: './image-loader.component.html',
  styleUrls: ['./image-loader.component.scss'],
})
export class ImageLoaderComponent implements OnInit {
  //#region 'Input, Output, ViewChild'
  @Input() userName = '';
  @Output() fileDataEmit = new EventEmitter<FileData>();
  //#endregion 'Input, Output, ViewChild'

  //#region 'Variables'
  public errorFileSize = false;
  public errorFileExtension = false;
  public file = '';
  public fileExtensionAccept = ['jpg', 'png', 'gif', 'jpg', 'jpeg', 'bmp'];
  public maxFileSize = 1024 * 1024 * 4;
  //#endregion 'Variables'

  //#region 'Angular Life Cycle'
  constructor() {}

  ngOnInit(): void {}
  //#endregion 'Angular Life Cycle'

  //#region 'Load'
  //#endregion 'Load'

  //#region 'General Methods'
  //#endregion 'General Methods'

  //#region 'Validations'
  readFile(fileData: any): void {
    this.errorFileExtension = false;
    this.errorFileSize = false;
    const file = fileData.target.files[0];
    const ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
    if (file.size > this.maxFileSize) {
      this.errorFileSize = true;
      this.fileDataEmit.emit({name: '', size: ''});
      this.file = '';
      return;
    }
    if (!this.fileExtensionAccept.includes(ext)){
      this.errorFileExtension = true;
      this.fileDataEmit.emit({name: '', size: ''});
      this.file = '';
      return;
    }
    const sizeMb = (file.size / 1048576).toFixed(2);
    this.fileDataEmit.emit({name: file.name, size: `${sizeMb} Mb`});
  }
  //#endregion 'Validations'
}
