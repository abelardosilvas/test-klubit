import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import {AbstractControl, Form, FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Moment} from "moment";

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss'],
})
export class FormularioComponent implements OnInit {
  //#region 'Variables'
  public dteInicio: NgbDateStruct;
  public dteFin: NgbDateStruct;
  public form: FormGroup;
  public patterOnlyText = '[a-zA-ZáéíóúÁÉÍÓÚñÑ]+(?: [a-zA-ZáéíóúÁÉÍÓÚñÑ]+)*';
  public patterOnlyNumbers = '[0-9]*';
  public errorOnlyText = 'El campo solo permite letras Mayusculas y Minusculas, espacios en blanco . y ,';
  public errorOnlyNumbers = 'El campo solo permite numeros';
  public errorInvalidEmail = 'Introduce un email valido';
  public dateNow = new Date();
  //#endregion 'Variables'

  //#region 'Angular Life Cycle'
  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      name: ['', Validators.pattern(this.patterOnlyText)],
      surnames: ['', Validators.pattern(this.patterOnlyText)],
      age: ['', Validators.pattern(this.patterOnlyNumbers)],
      cellphone: ['', Validators.pattern(this.patterOnlyNumbers)],
      email: ['', Validators.pattern(
        '(([^<>()[\\]\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})'
      )],
      password: [''],
      confirmPassword: [''],
      scheduleTrip: [false],
      startDate: [{day: this.dateNow.getDate(), month: this.dateNow.getUTCMonth() + 1, year: this.dateNow.getFullYear()}],
      endDate: [{day: this.dateNow.getDate() + 1, month: this.dateNow.getUTCMonth() + 1, year: this.dateNow.getFullYear()}],
      comments: [''],
      fileName: [''],
      fileSize: ['']
    }, {validators: [this.checkPasswords, this.checkDate]});
  }

  ngOnInit(): void {}
  //#endregion 'Angular Life Cycle'

  //#region 'Load'
  //#endregion 'Load'

  //#region 'General Methods'
  get name(): AbstractControl {
    return this.form.get('name');
  }
  get surnames(): AbstractControl {
    return this.form.get('surnames');
  }
  get age(): AbstractControl {
    return this.form.get('age');
  }
  get cellphone(): AbstractControl {
    return this.form.get('cellphone');
  }
  get email(): AbstractControl {
    return this.form.get('email');
  }
  get password(): AbstractControl {
    return this.form.get('password');
  }
  get confirmPassword(): AbstractControl {
    return this.form.get('confirmPassword');
  }
  get scheduleTrip(): AbstractControl {
    return this.form.get('scheduleTrip');
  }
  get startDate(): AbstractControl {
    return this.form.get('startDate');
  }
  get endDate(): AbstractControl {
    return this.form.get('endDate');
  }
  get comments(): AbstractControl {
    return this.form.get('comments');
  }
  get fileName(): AbstractControl {
    return this.form.get('fileName');
  }
  get fileSize(): AbstractControl {
    return this.form.get('fileSize');
  }
  getData({name, size}: {name: string, size: string}): void{
    this.fileName.setValue(name);
    this.fileSize.setValue(size);
  }
  getDateFormat(data: NgbDateStruct): Moment{
    const date = new Date(data.year, (data.month - 1), data.day);
    return moment(date);
  }
  //#endregion 'General Methods'

  //#region 'Validations'
  checkPasswords(group: FormGroup): {notSame: boolean} | null {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }
  numberOnly(event: KeyboardEvent): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  checkDate(group: FormGroup): any {
    const dateNow = moment(new Date().setHours(0,0,0,0));
    const startDateInput = group.controls.startDate.value;
    const endDateInput = group.controls.endDate.value;
    const startDate = moment(new Date(startDateInput.year, startDateInput.month - 1, startDateInput.day).setHours(0,0,0,0));
    const endDate = moment(new Date(endDateInput.year, endDateInput.month - 1, endDateInput.day).setHours(0,0,0,0));
    startDate > endDate ?
      group.controls.startDate.setErrors({...group.controls.startDate.errors, startDateHigherEndDate: true }) :
      group.controls.startDate.setErrors({...group.controls.startDate.errors, startDateHigherEndDate: false });
    startDate < dateNow ?
      group.controls.startDate.setErrors({...group.controls.startDate.errors, startDateLessDateNow: true }) :
      group.controls.startDate.setErrors({...group.controls.startDate.errors, startDateLessDateNow: false });
    endDate < startDate ?
      group.controls.endDate.setErrors({...group.controls.endDate.errors, endDateLessStartDate: true }) :
      group.controls.endDate.setErrors({...group.controls.endDate.errors, endDateLessStartDate: false });
  }
  //#endregion 'Validations'
}
